#!/bin/sh
cd  /opt/epos/YOURFOLDER_A/DailyProcedures
rm -f /opt/epos/software/epos_scripts/daily_files_monitorization/logcron.log
export PYTHONIOENCODING=utf-8
echo "Starting DailyProcedures cronjob at "`date` >> /opt/epos/logs/daily_procedures.log
echo "Starting a CronJob at "`date` >> /opt/epos/software/epos_scripts/daily_files_monitorization/logcron.log
rnxFiles=`ls -al /YOURFOLDER_B/BUCKET | grep .gz | wc -l`
echo "Quick Bash ls check detected $rnxFiles Rinex Files in the Bucket" >>  /opt/epos/logs/daily_procedures.log
/usr/bin/python2 /opt/epos/YOURFOLDER_A/DailyProcedures/DailyProcedures.py /opt/epos/YOURFOLDER_A/DailyProcedures/DailyProcedures.cfg >> /opt/epos/logs/daily_procedures.log
echo "DailyProcedures cronjob finished at "`date` >> /opt/epos/logs/daily_procedures.log
echo "CronJob Finished at "`date` >> /opt/epos/software/epos_scripts/daily_files_monitorization/logcron.log
