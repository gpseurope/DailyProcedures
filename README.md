## DailyProcedures

Standard Rinex File work Flow Processing for EPOS.

This project takes incoming RINEX files from a "Bucket" directory and processes the T2 and T3 EPOS metedata in a standard way.

DailyProcedures is a Python App that automatically processes RINEX files into a GLASS infrastructure. The workflow of the application can be found in the *DailyProcedures/docs* directory.

## Requirements 

- Python
- The Glass infrastructure:
	- Glass Framework Web Services
	 - RunQC available at [https://gitlab.com/gpseurope/RunQC](https://gitlab.com/gpseurope/RunQC)
	 - IndexGD available at [https://gitlab.com/gpseurope/indexGD](https://gitlab.com/gpseurope/indexGD)
	 - FWSS available at [https://gitlab.com/gpseurope/fwss](https://gitlab.com/gpseurope/fwss)

## How to run the tool 

**First**, make a new configuration file **DailyProcedures.cfg** accordingly to your needs. Make sure that you create the directores defined prior to the execution of this tool.

**Second**, you can manually start the tool as follows:
```$ python DailyProcedures.py```
or, preferably, use crontab to run the program.
 

## License

This project is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). 

## Acknowledgments

* This project has received funding from the European Union's Horizon 2020 research and innovation programm under grant agreement N° 676564


Warnings
The orbit directory is HARDCODED into the code and all orbit files are placed here (in case of reprocessing and also avoids multiple downloads for each file)
brdc = "/media/epos_ftp_share/ORBITS"
change this and other RunQc options directly in the code - file : DailyProcedures.py
For ftp - http   orbit repos. see the RunQC project code


