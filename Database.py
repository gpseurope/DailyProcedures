import sys, os, psycopg2

""" Execute SQL Query
    Args:
      query (string): SQL Query
      conf: Configuration object
    Returns:
      Returns results or None
"""
def Execute(query, conf):
  try:
    # Start connection to the database
    connection = Connect(conf)
    cursor = connection.cursor()
    # Execute parsed query
    cursor.execute(query)
    rows = cursor.fetchall()
    # print(rows)
    # Close cursor and connection
    cursor.close()
    connection.close()
    # Retun results
    if (len(rows) == 0):
      return (None)
    else:
      return(rows)
  except Exception as e: 
    return(None)

""" Connects to a postgresql database
    Args:
      conf: Configuration object
    Returns:
      Returns connection object or None
"""
def Connect(conf):
  try:
    connection_str =  "dbname='" + conf.get("database","db_name") + "' user='" + conf.get("database","db_username") + "' host='" + conf.get("database", "db_hostname") + "' " + \
                      "password='" + conf.get("database","db_password") + "' port='"+ conf.get("database","db_port") + "'"
    connection = psycopg2.connect(connection_str)
    return(connection)
  except Exception as e: 
    # print(e)
    return(None)
    