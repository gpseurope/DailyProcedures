import sys, os, subprocess, shutil, glob, hashlib,subprocess
from shutil import copyfile
# FIX Directory of RINEX files by moving files to a new directory

directory="/media/epos_ftp_share/RINEX2/30s/2019/"

def removeSubFolders(path):
  for root, dirs, files in os.walk(path):
    for dir_ in dirs:
        subfolder = path + "/" + dir_
        print("Deleting subfolder:" + subfolder)
        subprocess.call("rm -rf " + subfolder + "/", shell=True)

for root, dirs, files in os.walk(directory):
    for dir_ in dirs:
        if (str(dir_).isdigit() == True):
            print(dir_)
            # Move Files of the subdirectories [day of year] to the [day of the year] directory
            fromDir = directory + dir_ + "/*/*"
            toDir = directory + dir_ + "/"
            print("From Directory:" + fromDir)
            print("To Directory:" + toDir)
            subprocess.call("mv " + fromDir + " " + toDir, shell=True)
   
            # remove empty subfolders of [day of the year] folder
            removeSubFolders(directory + dir_ )