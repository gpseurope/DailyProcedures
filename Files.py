import sys, os, subprocess, Utils, shutil, glob, hashlib
from shutil import copyfile

class Rinex:
  def __init__(self, filename, directory, parent, conf):
    debug=True
    self.filename         = filename                        # Rinex filename
    self.parent           = parent
    self.directory        = directory.replace(parent, "")
    self.absolutePath     = self.parent + self.directory  
    self.md5checksum      = ""
    self.marker           = ""                              # Station marker, parsed from the rinex filename
    self.year             = ""                              # Year
    self.oyear            = ""                              # Original filename Year before parsing
    self.day_of_the_year  = ""                              # Day of the Year
    self.status           = 0                               # QC status flag
    self.log              = ""                              # Rinex Log filename 
    self.file_type        = self.typeOf()                   # Rinex file type
    self.setParameters()                                    # Get Rinex Parameters
    #self.relativePath     = self.year + "/" + self.day_of_the_year + "/" + self.marker + "/"
    self.relativePath     = self.year + "/" + self.day_of_the_year + "/"
    self.conf             = conf
    
    if (debug == True):
      print(" # Filename: " + self.filename)
      print(" # Compressed MD5 Checksum: " + self.md5checksum)
      print(" # Type: " + self.file_type)
      print(" # YEAR/DOY: " + self.year + "/" + self.day_of_the_year)
      # Directory var will be empty if the rinex files are on the root of the bucket directory
      print(" # Directory: " + self.directory)
      print(" # Relative Path: " + self.relativePath)
      print(" # Absolute Path: " + self.absolutePath)   

  """ Get rinex type
  """
  # uncompress file and read header to know type 
  def typeOf(self):
    # Write uncompressed content to the stdout
    try:
    	header = (subprocess.check_output("gunzip -c " + self.absolutePath + self.filename, shell=True)[:1000]).splitlines()
    except Exception as error: print(error)
    # Based on the implementation in INDEXGD
    for line in header:
      if line.find('RINEX VERSION') > -1:
        r_vers = line.split()[0]
        if float(r_vers) < 3:
          return ('RINEX2')
        else:
          return ('RINEX3')
        break
      # TODO: REMOVE UNCOMPRESSED FILE
        
  # Set rinex parameters, year, doy, marker etc
  def setParameters(self):
    if (self.file_type in ['RINEX2', 'RINEXN2', 'RINEXM2']):
      self.marker = self.filename[0:4]
      self.year   = self.filename[9:11]
      self.oyear  = self.year
      self.day_of_the_year  = self.filename[4:7]
 
      # Only for Rinex2
      if float(self.year) > 80:
        self.year = "19" + self.year
      else:
        self.year = "20" + self.year

    if (self.file_type in ['RINEX3', 'RINEXN3', 'RINEXM3']):
      self.marker = self.filename[0:4]
      self.year   = self.filename[12:16]
      self.oyear  = self.year 
      self.day_of_the_year = self.filename[16:19]

    # Set md5 checksum of current RINEX object file
    self.md5checksum = hashlib.md5(open(self.absolutePath + self.filename, "rb").read()).hexdigest()
    
 
 
  """ Moves the rinex file and its log to a directory
    Args:
      toDir (string): Move the current rinex file to this directory
  """
  def move(self, toDir):    
    # Kinda, but not realy, recursively create directories 
    # It will create the same set of directories, for current the rinex file, that was available on the bucket directory
    # e.g. /bucket/2019/001/CASC0010.19D.Z -> /history/2019/001/CASC/CASC0010.19D.Z
    folders = []
    prev_folder = ""
    folders = os.path.normpath(self.relativePath).split(os.sep)
    for folder in folders:
      folder = prev_folder + "/" + folder
      prev_folder = folder
      #print("Creating directory = " + os.path.normpath(toDir + folder))
      if not os.path.exists(toDir + folder): os.mkdir(os.path.normpath(toDir + folder))
    # Fail-safe remove files first because rename will not overwrite
    try:
      os.remove(toDir + self.filename + "") 
      os.remove(toDir + self.filename + ".log") 
    except Exception as error: pass
    
    # Moving stage:
    # print ("# Moved Files FROM the following directory:" + self.absolutePath)
    # print ("# Moved files TO the following directory:" + toDir + self.relativePath)
    
    # 1- Move RINEX File to the new directory
    os.rename(self.absolutePath + self.filename, toDir + self.relativePath + self.filename)  

    # 2- Move RINEX Log file to the new directory
    if (os.path.isfile(self.absolutePath + self.filename + ".log")): ## Check if log file exists
      os.rename(self.absolutePath + self.filename + ".log", toDir + self.relativePath + self.filename + ".log")  
    
    # 3- Move RUNQC Log File and output to the new directory
    for QCLog in glob.glob(os.path.join("tmp/QC/log/", '*.*')):
      shutil.move(QCLog, toDir + self.relativePath + self.filename + "-QC.xml")
    for QCOutput in glob.glob(os.path.join("tmp/QC/output/", '*.*')):
      shutil.move(QCOutput, toDir + self.relativePath + os.path.basename(QCOutput))

    # 4- Copie lines of the FWSS log that correspond to errors related to the current rinex file
    fwss = self.conf.get("programs", "fwss")
    fwss_log_queries    = fwss[0:fwss.rfind('/')]+"/log/queries.log"
    fwss_log_qc_queries = fwss[0:fwss.rfind('/')]+"/log/qc_queries.log"
     
    if (os.path.isfile(fwss_log_qc_queries) == True):
      with open(fwss_log_qc_queries) as f1:
        with open(toDir + self.relativePath + self.filename + ".log","a") as f2:
          for line in f1:
            if ("INFO" not in line):
              f2.write(line)
      # Remove file from the fwss log directory
      os.remove(fwss_log_qc_queries)
    #
    if (os.path.isfile(fwss_log_queries) == True):
      with open(fwss_log_queries) as f1:
        with open(toDir + self.relativePath + self.filename + ".log","a") as f2:
          for line in f1:
            if ("INFO" not in line):
              f2.write(line)
      # Remove file from the fwss log directory
      os.remove(fwss_log_queries)
    
     
    
    # Alternative implementation (1)    
    #shutil.move(fwss_log_queries, toDir + self.relativePath + "queries.log")
    #shutil.move(fwss_log_qc_queries, toDir + self.relativePath + "qc_queries.log")

    # Alternative implementation (2)
    #fwss = self.conf.get("programs", "fwss")
    #with open(fwss[0:fwss.rfind('/')]+"/log/queries.log") as file:
    #  for line in file:
    #    if self.marker in line:
    #      if (self.day_of_the_year in line):
    #        if (self.oyear in line):
    #          ofile = open(toDir + self.relativePath + self.filename + "-fwss.log", "a")
    #          ofile.write(line)
    #          ofile.close()
              

  """ Copies the rinex file to a directory () - Legacy support only, do not use this function (see move())
    Args:
      toDir (string): Copy the rinex file to this directory
  """
  def copy(self, toDir):    
    # Kinda, but not realy, recursively create directories
    folders = []
    prev_folder = ""
    folders = os.path.normpath(self.relativePath).split(os.sep)
    for folder in folders:
      folder = prev_folder + "/" + folder
      prev_folder = folder
      #print("Creating directory = " + os.path.normpath(toDir + folder))
      if not os.path.exists(toDir + folder): os.mkdir(os.path.normpath(toDir + folder))
    
    # Copy RINEX file to the new directory
    copyfile(self.absolutePath + self.filename, toDir + self.relativePath + self.filename)
    # Copy RINEX Log file to the new directory
    if (os.path.isfile(self.absolutePath + self.filename + ".log")): ## Check if log file exists
      copyfile(self.absolutePath + self.filename + ".log", toDir + self.relativePath + self.filename + ".log")  


