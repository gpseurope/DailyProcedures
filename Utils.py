import sys, os, subprocess, time, psycopg2, urllib, json, smtplib, urllib

""" Custom enum """
def enum(**enums): return type('Enum', (), enums)

""" Adds '/' to the end of a path
    Args:
      path (string): path to be fixed 
    Returns (string):
      Fixed path
"""
def dirFix(path):
    if not path.endswith('/'): path += '/'
    return(path)

""" Removes a char at the end of a string
    Args:
      str (string): input string
      ending (char): char to be removed from the end of the input string
    Returns (string):
      String without the ending char
"""
def rchop(str, ending):
    if str.endswith(ending):
        return str[:-len(ending)]
    return str

""" Makes a nice conole waiting animation
    Args:
      text (string): Animation text
"""
def animation(text):
    chars = "|/-\\"
    for i in range(100):
        time.sleep(0.1)
        sys.stdout.write("\r" + text + " " + chars[i % len(chars)])
        sys.stdout.flush()
    # Removes last output and goes back to the beginning of the line
    sys.stdout.write("\x1b[2K\r")
    sys.stdout.write(text)
    sys.stdout.flush()
    
""" Custom waiting/sleep function
    Args:
      text (string) : Animation text
      duration (int): How long to wait in seconds
"""
def wait(text, duration):
    print("")
    start_time = time.time()
    while True:
        elapsed_time = time.time() - start_time
        animation(text)
        if (elapsed_time >= duration): break

""" Makes a request to a Web Service
    Args:
      endpoint (string): Web Service endpoint
      conf (Python Object): Configuration object
    Returns:
      Array of JSON Python Objects
"""
def webServiceRequest(endpoint, conf):
  try:
    ws_url = conf.get("webservices", "ws_url") 
    if not ws_url.endswith('/'): ws_url += '/'
    # Construct 
    url =  ws_url + endpoint
    response = urllib.urlopen(url)
    data = json.loads(response.read())
    return data
  except Exception as error: return(None)

""" Updates GLASS-API Cache
    Args:
      conf (Python Object): Configuration object
    Returns:
      True if the cache was successfully updated, false, otherwise.
"""
def updateRemoteGlassApiCache(conf):
  try:
    ws_url = conf.get("webservices", "ws_url") 
    if not ws_url.endswith('/'): ws_url += '/'
    url =  ws_url + conf.get("webservices", "updateCache")
    response = urllib.urlopen(url)
    data = response.read()
    # print(data)
    if (data.find("Success") >= 0): 
      return True 
    else:
      return False
  except Exception as error: return(False)

""" Gets a rinex file using the Glass framework (WS)
    Args:
      rinex: Rinex object
      conf: Configuration object 
    Returns:
      JSON Python Object
"""
def getRinex(conf, rinex):
  # Rinex Web Service endpoint for the current rinex file
  endpoint = "webresources/files/file_name/" + rinex.filename +"/json?md5="+rinex.md5checksum
  print(endpoint)
  return(webServiceRequest(endpoint, conf))

def getMarker(conf, rinex):
  # Rinex Web Service endpoint for the current rinex file
  endpoint = conf.get("webservices", "marker")+"?marker="+rinex.marker
  print(endpoint)
  return(webServiceRequest(endpoint, conf))  

""" Gets files of a directory
    Args:
      dir (string): Target directory
    Returns (Array):
      Array of filenames (string)
"""
def getFilesOf(dir):
  filenames = []
  for r, dirs, f in os.walk(dir):
    for file in f:
      filenames.append(f)
  return(filenames)

""" Remove Empty Folders
    Args:
      path (string): Is the path
      removeRoot (Boolean): Remove root directory
"""
def removeEmptyFolders(path, removeRoot=True):
  debug=False
  if not os.path.isdir(path): return
  # remove empty subfolders
  files = os.listdir(path)
  if len(files):
    for f in files:
      fullpath = os.path.join(path, f)
      if os.path.isdir(fullpath):
        removeEmptyFolders(fullpath)

  # if folder empty, delete it
  files = os.listdir(path)
  if len(files) == 0 and removeRoot:
    if (debug == True): print ("Removing empty folder:", path)
    os.rmdir(path)

""" Get file size
    Args:
      file_path (string): filename
    Returns:
      Returns size of file in bytes
"""
def getFileSize(file_path):
    if os.path.isfile(file_path):
        file_info = os.stat(file_path)
        return file_info.st_size

""" Check if URL is reachable
    Args:
      URL (string): URL to test
    Returns:
      Returns True if URL is reachable
"""
def isURLReachable(URL):
  try:
    if (urllib.urlopen(URL).getcode() == 200): 
      return(True)
  except Exception as error: pass
  return(False)

""" Convert string(=True or False) to boolean
    Args:
      v (string): input string
    Returns:
      Returns True or False
"""
def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")
