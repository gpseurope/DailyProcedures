# -*- coding: utf-8 -*-
#   ----------------------------------------------------------------------------------------
#   DailyProcedures - Python App that processes RINEX files into a GLASS infrastructure
#   ----------------------------------------------------------------------------------------
#   Authors:
#       Tiago Simoes  (tiagomiguelcs@segal.ubi.pt)
#       Paul Crocker  (crocker@segal.ubi.pt)
#       Rui Fernandes (rui@segal.ubi.pt)
#   Created:          June 2019
#   Last modified:    Feb 2020
#   ----------------------------------------------------------------------------------------
import sys, os, subprocess, Files, shutil, Utils, time, logging, Database, datetime, tempfile
from ConfigParser import SafeConfigParser
# Program version
version = "0.4"
# Define programs to be used
Program = Utils.enum(INDEXGD=1,RUNQC=2,FWSS=3,GENERIC=4,DATABASE=5)
# File Objects
rinex_objects = []
json_objects  = []

cwd = os.getcwd()
indexgdtmp =  tempfile.mktemp( prefix="tmpindexgd",  dir=cwd )

""" Logs the stdout and stderr of each external program
    Args:
      Program (enum): Log of what program
      file (string): The filename of the log to be created
    Returns (string):
      File Object
"""
def customLogging(program, file):
  fo = open(file, 'a')
  strline = "######################################\n"
  if (program == Program.INDEXGD):
      strline += "INDEXGD - "  
  if (program == Program.RUNQC):
    strline += "RUNQC - "
  if (program == Program.GENERIC):
    strline += "GENERIC - "
  if (program == Program.FWSS):
    strline += "FWSS - "
  strline += time.strftime("%d/%m/%Y %H:%M:%S") + ":"
  strline += "\n######################################"
  fo.write(strline+"\n")
  fo.close()
  fo = open(file, 'a')
  return(fo)

""" Runs an external program
    Args:
      Program (enum): External program to be run
      rinex (string): Rinex filename to be parsed to the program
    Returns (string):
      True if the program was successful, or false otherwise
"""
def runProgram(program, rinex, conf):
  debug=True # Debug flag
  if (program == Program.FWSS):
    # Check if the program is already running on the background if not, start it
    variable = subprocess.Popen("ps -ef", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = variable.communicate()
    if ("web_server.py" in stdout): 
      print("# FWSS is already running, continuing.")
      # Tip. Use pkill -f web_server.py to stop the process
      return True
    
    fwss = conf.get("programs", "fwss")
    # Get directory of the fwss program
    fdir = fwss[0:fwss.rfind('/')+1]
    # We need to change to the program's directory to avoid issues with the configuration file (web_server.cfg)
    # Run FWSS as a background process and create a log file with the output of the program
    command = "cd " + fdir + " && python2 " + fwss + " &> status.log &"
    if (debug == True): print(command)

  if (program == Program.INDEXGD):
    print(" # Runing INDEXGD...")
    # Create a list of files with only the current rinex file to be processed
    tmp_f = indexgdtmp
    f = open(tmp_f, "w")
    f.write(rinex.absolutePath + rinex.filename)
    f.close()
    bin     = "python2 " + conf.get("programs","indexgd")
    pars    = " -l " + tmp_f + " -t RINEX2 -s 24h -f 30s -d " + conf.get("DailyProcedures", "dataCenter") + " -r %YYYY%/%DOY% -w " + Utils.rchop(conf.get("FWSS", "f_url"),"/") + "/gps/data/rinex" 
    command = bin + pars
    if (debug == True): print(command)

  if (program == Program.RUNQC):
    print(" # Running RUNQC...")
    bin      = "/bin/perl " + conf.get("programs","runqc") 
    # Need to make a variable
    # brdc = tmp/QC/output
    brdc = "/mnt/glass_glassutils/ORBITS"
    pars     = " --save_dir tmp/QC/log --brdc_dir " + brdc  + " --time_lim 180 --mask_rxo " + rinex.absolutePath + rinex.filename + " --post_api " + Utils.rchop(conf.get("FWSS", "f_url"),"/")
    command  = bin + pars
    if (debug == True): print(command)

  # Execute Program selected and handle errors
  try:
    # Hide stdout output
    FNULL = open(os.devnull, 'w')
    # Special case, FWSS
    if (program == Program.FWSS):
      subprocess.check_call(command, shell=True, stdout=FNULL)
      # Continue only if FWSS is running
      # -> Check the status.log, previously created, to know this!
      fdir = conf.get("programs", "fwss")[0:conf.get("programs", "fwss").rfind('/')+1]
      start_time = time.time()
      while True:
        elapsed_time = time.time() - start_time
        Utils.animation("# Starting FWSS")
        #print(elapsed_time)
        with open(fdir+"status.log") as status:
          if "PIN" in status.read():
             time.sleep(2)
             break
        if (elapsed_time >= 5): 
          print("") 
          return False # break after after 5 seconds without any changes
      print("")
    else:
      subprocess.check_call(command, shell=True, stdout=customLogging(program, rinex.absolutePath + rinex.filename + ".log"), stderr=subprocess.STDOUT)
    
    # Specifically using Process Error Exception - Exception do not always "catches" everything  
    # SubprocessError, raised when a process ran by check_call() returns a non-zero exit status.
  except (Exception, OSError, subprocess.CalledProcessError) as error:
    print(" # Error: {0}".format(error))    
    return False
  #
  return True

""" Minimum Requirements checks
    Args:
      conf (Python Object): Configuration object
    Returns (string):
     True if the requirements were met, false otherwise
"""
def reqChecks(conf):

  print("  # Running Requirements Checks...")

  # Check if we can connect to the database  
  if (Utils.str2bool(conf.get("DailyProcedures", "useWS")) == False):
    connection = Database.Connect(conf)
    if (connection == None):
      print("# Can't connect to the database.")
      wLog(conf, "DailyProcedures.log").error("- [DATABASE]: Can't connect to the database. Please, revise the configuration file.")
      print("# Exiting application...")
      exit(conf, 1)
    connection.close()
  
  # Check if program INDEXGD exists
  if (os.path.isfile(conf.get("programs","indexgd")) == False):
     print("# INDEXGD program not found: No such file or directory.")
     wLog(conf, "DailyProcedures.log").error("- [INDEXGD]: Program not found: No such file or directory.")
     print("# Exiting application...")
     exit(conf, 1)
  
  # Check if program RUNQC exists
  if (os.path.isfile(conf.get("programs","runqc")) == False):
     print("# RUNQC program not found: No such file or directory.")
     wLog(conf, "DailyProcedures.log").error("- [RUNQC]: Program not found: No such file or directory.")
     print("# Exiting application...")
     exit(conf, 1)

  # Check if Glass Framework Web Service is reachable
  if (Utils.isURLReachable(conf.get("webservices", "ws_url")) == False):
    print("# Web Service '" + conf.get("webservices", "ws_url") + "' is not reachable. Please, revise the configuration file.")
    wLog(conf, "DailyProcedures.log").error("- [GENERIC]: Web Service '" + conf.get("webservices", "ws_url") + "' is not reachable. Please, revise the configuration file.")
    print("# Exiting application...")
    exit(conf, 1)

  # Check if is possible to update Glass API Cache and Update it!
  print("  # Updating Glass API Cache. Address " + conf.get("webservices", "ws_url") + conf.get("webservices", "updateCache") )
  if (Utils.updateRemoteGlassApiCache(conf) == False):
    print("# Fail on updating Glass API Cache with address, Please, see the log file for more details.")
    wLog(conf, "DailyProcedures.log").error("- [GENERIC]: Fail on updating Glass API Cache with address " + conf.get("webservices", "ws_url") + "" + conf.get("webservices", "updateCache") + " - Please, revise the configuration file.")
    print("# Exiting application...")
    exit(conf, 1)
  
  print("  # Req Checks Finished ok");

  
""" Exit procedures that includes a cleaning stage
    Args:
      conf (int): sys.exit code
"""
def exit(conf, code):
  # End cleaning stage
  print("# Cleaning.")
  # 1 - Remove empty directories of the bucket
  bucket_directory = Utils.dirFix(conf.get("directories", "dir_bucket"))
  Utils.removeEmptyFolders(bucket_directory, False)
  # 2 - Remove tmp directories
  if os.path.exists(os.getcwd() + "/" + "tmp/"): 
    shutil.rmtree(os.getcwd() + "/" + "tmp/")
  # 3 - Remove old status.log generated by fwss (avoid large log file)
  fwss = conf.get("programs", "fwss")
  if (os.path.isfile(fwss[0:fwss.rfind('/')]+"/status.log")): 
    os.remove(fwss[0:fwss.rfind('/')]+"/status.log")
  
  print("# Finished.")
  sys.exit(code)


""" Write to Global Log
    Args:
      conf         : Configuration object
      file (string): The filename of the log to be created
"""
def wLog(conf, filename):
  fm='a'
  if (os.path.isfile(filename)):
    # Get size of file
    size = float(Utils.getFileSize(filename))
    # Convert to megabytes
    size = (size / 1024.0) / 1024.0
    if (size > float(conf.get("DailyProcedures", "maxLogSize"))):
      print("# The Log file will be recreated (max log size reached)")
      fm='w'
  else: fm='w'
  
  logging.basicConfig(filename=filename, filemode=fm, format='%(asctime)s %(levelname)s %(message)s', datefmt='%Y-%m-%d %H:%M:%S')
  return(logging.getLogger(filename))

""" Main DailyProcedures Function """
def main():
  count_success = 0
  count_fails = 0
  # Cleaning stage
  if os.path.exists(os.getcwd() + "/" + "tmp/"): shutil.rmtree(os.getcwd() + "/" + "tmp/")
  # os.system('clear')
  
  # 0 - Read Configuration File
  # This is the default
  cfile="DailyProcedures.cfg"

  # Check that user has not suppled command line option
  if ( len(sys.argv) == 2 ):
    cfile=sys.argv[1]

  # Check cfg file exists
  if (os.path.isfile( cfile ) == False):
    print("# Configuration file not found. " +cfile)
    sys.exit(1)
  
  print("# config file is " + cfile)

  # Read and Parse the cfg file
  conf = SafeConfigParser()
  conf.read( cfile )
  
  #StartUP
 
  print("-----------------------")
  now = datetime.datetime.now()
  print(" DailyProcedures v" + version + " Startup at : "+ now.strftime("%Y-%m-%d %H:%M:%S"))
  wLog(conf, "DailyProcedures.log").warn(" DailyProcedures v" + version + " Startup" )
       
  #print("  Using Python v" + sys.version + "\n\n")
  print("-----------------------")
  
  
  useWS = Utils.str2bool(conf.get("DailyProcedures", "useWS"))
  if (useWS == True):
    print("# Using Web Services instead of Database direct access.")
  else:
    print("# Using Database direct access.")

  # Fail-safe, remove FWSS log before any run. We need a FWSS log only with data of the current
  # rinex file being processed - If necessary, this log will be later moved to the Fail directory.
  print("# Cleaning FWSS Logs.")
  fwss                = conf.get("programs", "fwss")
  fwss_log_queries    = fwss[0:fwss.rfind('/')]+"/log/queries.log"
  fwss_log_qc_queries = fwss[0:fwss.rfind('/')]+"/log/qc_queries.log"
  if (os.path.isfile(fwss_log_queries) == True): os.remove(fwss_log_queries)
  if (os.path.isfile(fwss_log_qc_queries) == True): os.remove(fwss_log_qc_queries)

  # 1 - Check all requirements of the application, if not fulfilled, exit with error
  reqChecks(conf)

  # 2 - Start FWSS
  if (runProgram(Program.FWSS, "", conf) == False): 
    print("# Failure to start FWSS")
    # Write to global Log
    wLog(conf, "DailyProcedures.log").error("- [FWSS]: Failure to start FWSS.")   
    exit(conf, 1)

  # 2.1 - After FWSS start, check if FWSS web server is reachable 
  if (Utils.isURLReachable(conf.get("FWSS", "f_url")+"gps") == False):
    print("# Web Server '" + conf.get("FWSS", "f_url") + "gps' FWSS is not reachable. Please, revise the configuration file.")
    wLog(conf, "DailyProcedures.log").error("- [GENERIC]: Web Server '" + conf.get("FWSS", "f_url") + "gps' is not reachable. Please, revise the configuration file.")
    print("# Exiting application...")
    exit(conf, 1)

  # 3 - Iterate Rinex files by reading the bucket directory
  emptyBucket=True
  
  bucket_directory = Utils.dirFix(conf.get("directories", "dir_bucket"))

  fileMarkerOk=False
  
  for r, dirs, f in os.walk(bucket_directory):
    # Run Programs for each file of the bucket directory and POST to FWSS / DB-API
    
    ##sort by alphabetical name. Could also sort by day
    f.sort()

    for file in f:
      
      if not file.lower().endswith((".z", ".gz", ".7z")): continue # File extensions allowed
      
      fullfile = bucket_directory+file
      #fullfile = bucket_directory+"/"+file
      print("Processing File "+fullfile)
   
      try:
        if (emptyBucket == True): emptyBucket=False
        ##################
        # Check if the file is not corrupted
        #
        header = (subprocess.check_output( ["gunzip","-c",fullfile] ) ).splitlines()
        print("Header Gunzip is OK")

        try:
          rinex_file = Files.Rinex(file, os.path.join(r, ""), bucket_directory, conf)
        except:
          print ("creating rinex object failed for " + fullfile)
          failDirectory = Utils.dirFix(conf.get("directories", "dir_corrupt"))
          shutil.move(fullfile,failDirectory+"/"+file)
          continue

        ##Check is Station Marker is in local database
        ##Not handling exceptions
        ##print("Checking Marker Exists")
        if (useWS == True):
          result = Utils.getMarker(conf, rinex_file)
        else:
          result = Database.Execute("SELECT station FROM station WHERE marker='" + rinex_file.marker + "'", conf)
        if (result == None):
          print("Move to dir_unknown : Marker does not exists at Node "+file);
          failDirectory = Utils.dirFix(conf.get("directories", "dir_unknown"))
          shutil.move(fullfile,failDirectory+"/"+file)
          continue
        print("Marker Exists "+rinex_file.marker);


        ####################
        # Run INDEXGD - T2 #
        ####################
        print("# Processing '" + rinex_file.filename + "' (md5: '" + rinex_file.md5checksum + "')")
        # <!> Check for program errors
        if (runProgram(Program.INDEXGD, rinex_file, conf) == False):
          print(" # INDEXGD ended with an error")
          print("   # Rinex file '" + rinex_file.filename + "' was moved to the fail directory")
          failDirectory = Utils.dirFix(conf.get("directories", "dir_failT2"))
          # Write to global Log
          wLog(conf, "DailyProcedures.log").error("- [INDEXGD]: Fail on processing Rinex File '"+ rinex_file.filename + "'. Please, see the content in directory '" + failDirectory + rinex_file.relativePath  + "' for more details.")
          # Move file to the error directory
          rinex_file.move(failDirectory)
          continue # After a program error avoid to process file with RUNQC
  
        # Check if the rinex file was posted to the database using WS or by directly accessing the database
        if (useWS == True):
          result = Utils.getRinex(conf, rinex_file)
        else:
          result = Database.Execute("SELECT status FROM rinex_file WHERE name='" + rinex_file.filename + "' AND md5checksum='" + rinex_file.md5checksum + "'", conf)
      
        if result is None: # Rinex not in the database
          print(" # INDEXGD ended with an error: Rinex not in the database : POST failure")
          failDirectory = Utils.dirFix(conf.get("directories", "dir_failT2"))
          # Write to global Log
          wLog(conf, "DailyProcedures.log").error("- [INDEXGD]: Fail on posting Rinex File '"+ rinex_file.filename + "' to the database. Please, see the content in directory '" + failDirectory + rinex_file.relativePath  + "' for more details.")
          # Move file to the error directory
          rinex_file.move(failDirectory)
          continue # After a POST error avoid to process file with RUNQC
        else: 
          if (useWS == True):
            rinex_file.status = result["status"]
          else:
            rinex_file.status = result[0][0]
          
        ###################
        # Run RunQC - T3  #
        ###################

        # <!> Check for program errors
        if (runProgram(Program.RUNQC, rinex_file, conf) == False):
          print(" # RUNQC ended with an error")
          print("   # Rinex file '" + rinex_file.filename + "' was moved to the fail directory")
          failDirectory = Utils.dirFix(conf.get("directories", "dir_failT3"))
          # Write to global Log
          wLog(conf, "DailyProcedures.log").error("- [RUNQC]: Fail on processing Rinex File '"+ rinex_file.filename + "'. Please, see the content in directory '" + failDirectory + rinex_file.relativePath  + "' for more details.")
          # Move file to the error directory
          rinex_file.move(failDirectory)
          continue

        # <!> Check if the rinex file was updated using WS or by directly accessing the database
        if (useWS == True):
          result = Utils.getRinex(conf, rinex_file)
        else:
          result = Database.Execute("SELECT status FROM rinex_file WHERE name='" + rinex_file.filename + "' AND md5checksum='" + rinex_file.md5checksum + "'", conf)
      
        if (result == None):
          print(" # RUNQC ended with an error: POST failure, status was not retrievable for rinex '"+ rinex_file.filename +"'\n")
          failDirectory = Utils.dirFix(conf.get("directories", "dir_failT3"))
          # Write to global Log
          wLog(conf, "DailyProcedures.log").error("- [RUNQC]: Fail on posting Rinex File '"+ rinex_file.filename + "' to the database - Status flag was not retrievable. Please, see the content in directory '" + failDirectory + rinex_file.relativePath  + "' for more details.")
          # Move file to the error directory
          rinex_file.move(failDirectory)
          continue

        if (useWS == True):
          new_status = result["status"]
        else:
          new_status = result[0][0]

        if (new_status == rinex_file.status):
          print(" # RUNQC ended with an error: Possible POST failure, status was not changed for rinex '"+ rinex_file.filename +"'\n")
          failDirectory = Utils.dirFix(conf.get("directories", "dir_failT3"))
          # Write to global Log
          wLog(conf, "DailyProcedures.log").error("- [RUNQC]: Fail on posting Rinex File '"+ rinex_file.filename + "' to the database - Status flag was not changed. Please, see the content in directory '" + failDirectory + rinex_file.relativePath  + "' for more details.")
          # Move file to the error directory
          rinex_file.move(failDirectory)
          continue

        # Store the status flag value from the database
        rinex_file.status = new_status
      
        ######################
        # Flag Control - T3  #
        ######################

        # <!> Check QC STATUS flag for the current rinex file
        #     If flag < 0 move it to the error directory
        if (rinex_file.status < 0):
          count_fails += 1
          print(" # The QC is < 0 for rinex '"+ rinex_file.filename +"' (" + str(rinex_file.status) + ") moving file to defined T3 fail folder")
          customLogging(Program.FWSS, rinex_file.absolutePath + rinex_file.filename + ".log").write("<!> FAIL - The QC status is < 0 (" + str(rinex_file.status) + ") for rinex '"+ rinex_file.filename +"'. Please, see below for more info.\n")
          failDirectory = Utils.dirFix(conf.get("directories", "dir_failT3"))
          # Write to global Log
          wLog(conf, "DailyProcedures.log").error("- [FWSS]: The QC status for '"+ rinex_file.filename + "' is < 0 (" + str(rinex_file.status) + "). Please, see the content in directory '" + failDirectory + rinex_file.relativePath  + "' for more details.")
          # Move file to the error directory
          rinex_file.move(failDirectory)
        else:
	  count_success += 1
          print("# Rinex file processed with success. rinex_file.status="+str(rinex_file.status) )
          rinex_file.move(Utils.dirFix(conf.get("directories", "dir_success")))
     
      except OSError as e:
        count_fails += 1
        print("OSError:" + str(e))
        failDirectory = Utils.dirFix(conf.get("directories", "dir_corrupt"))
        print("gunzip crashed because " + fullfile + " is corrupted. Moving File to "+failDirectory )
        shutil.move(fullfile,failDirectory+"/"+file)

      except subprocess.CalledProcessError as error:
        count_fails += 1
        print("Sub Process Exception: "+ str(error.args) )
        failDirectory = Utils.dirFix(conf.get("directories", "dir_corrupt"))
        print("gunzip crashed because " + fullfile + " is corrupted. Moving File to "+failDirectory )
        shutil.move(fullfile,failDirectory+"/"+file)

      except Exception as error:
        count_fails += 1
        print("General Error from Unknown Exception. Skipping file")
        print(error.args)
  
  if (emptyBucket == True): print("# The bucket directory is empty.")
  wLog(conf, "DailyProcedures.log").warn(" DailyProcedures v" + version + " Finished All Processing" )
  
  #updating the cache
  print("Daily processing ending. Final update of local cache")
  print("Success = " + str(count_success))
  print("Fails = " + str(count_fails))
  Utils.updateRemoteGlassApiCache(conf) 

  if (os.path.isfile( indexgdtmp )) :
    os.remove( indexgdtmp )

  exit(conf, 0)

if __name__== "__main__":
  main()
